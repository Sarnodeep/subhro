<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesSectionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_section_type', function (Blueprint $table) {
            $table->unsignedInteger('page_id');
            $table->unsignedInteger('section_type_id');
            $table->smallInteger('order')->default(0);
            $table->longText('data');
            $table->timestamps();

            $table->foreign('page_id')
                ->references('id')
                ->on('pages')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('section_type_id')
                ->references('id')
                ->on('section_types')
                ->onDelete('cascade')
                ->onUpadte('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_section_type');
    }
}

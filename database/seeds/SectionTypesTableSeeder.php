<?php

use Illuminate\Database\Seeder;

class SectionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;
        while ($i < 6) {
            \App\Models\SectionType::create([
                'title' => "section_$i",
                'image' => "images/section_$i.png",
                'template_path' => "sections.section_$i"
            ]);
            $i++;
        }
    }
}

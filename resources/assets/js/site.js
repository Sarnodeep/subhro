export default {
    sliderCarousel() {
        "use strict";
        window.$('#sliderForHeading').slick({
            dots: false,
            autoplay: true,
            arrows: false,
            infinite: true,
            speed: 1000,
            cssEase: 'linear'
        });
    }
}
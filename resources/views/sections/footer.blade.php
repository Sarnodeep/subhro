<!-- Footer -->
<footer>

    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <span class="copyright">Copyright &copy; {!! env('APP_NAME') !!} {!! \Carbon\Carbon::now()->year !!}</span>
            </div>

            <div class="col-md-4">

                <ul class="list-inline social-buttons">
                    @foreach(['facebook', 'twitter', 'youtube', 'googlePlus'] as $item)
                        @if(env($item."Url"))
                            <li class="list-inline-item">
                                <a href="{!! env($item."Url") !!}">
                                    <i class="fa fa-{!! kebab_case($item) !!}"></i>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>

            </div>

            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li class="list-inline-item">
                        <a href="#">Privacy Policy</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">Terms of Use</a>
                    </li>
                </ul>
            </div>


        </div>
    </div>
</footer>
@extends('layouts.adminLayout')
@section('content')
    <div class="columns">

        <div class="column">

            {{--List of the --}}
            <div class="message">
                <div class="message-header">
                    <p>Edit <a href="{!! url($page->slug) !!}">{!! strtoupper($page->title) !!}</a> Page</p>
                </div>
                <div class="message-body">
                    {!! Form::model($page, ['method'=>'patch', 'route' => ['pages.update', $page->id]]) !!}
                    @include('admin.Page.form._form')
                    {!! Form::close() !!}
                </div>
            </div>

            {{--List of available sections--}}
            <div class="message">
                <div class="message-header">
                    <p>Add Section</p>
                </div>
                <div class="message-body">
                    <div class="content">
                        <figure>
                            @foreach($sectionTypes as $sectionType)
                                <a href="{!! url("admin/pages/$page->id/section/$sectionType->id") !!}">
                                    <img src="{!! url($sectionType->image) !!}" alt="{!! $sectionType->title !!}" class="image is-128x128" >
                                </a>
                            @endforeach
                        </figure>
                    </div>
                </div>
            </div>


        </div>

        {{--Added sections --}}
        <div class="column">
            @if(count($page->sections) == 0)
                <h4 class="title is-4">No Section Available</h4>
            @else
                <h4 class="title is-4">Already added sections</h4>
            @endif

            @foreach($page->sections as $section)
                @include('admin.Page.form.embeddedSection')
            @endforeach
        </div>


    </div>
@endsection
<div class="column">
    <div class="card">
        <header class="card-header">
            <p class="card-header-title">
{{--                {!! title_case($page->title) !!}--}}
            </p>

        </header>

        <div class="card-content">

            <figure class="image is-128x128">
                <img src="https://bulma.io/images/placeholders/256x256.png"
                     alt="Placeholder image">
            </figure>

            <div class="content">
                <br>
                Created at<time> {!! $page->created_at->diffForHumans() !!}</time>
                <br>
                Last Updated at<time> {!! $page->updated_at->diffForHumans() !!}</time>
            </div>
        </div>

        <footer class="card-footer">
            <a href="{!! url($page->slug) !!}" class="card-footer-item">View</a>
            <a href="{!! action('Admin\PageSectionController@show', [$page->id, $section->id]) !!}" class="card-footer-item">Edit</a>
            <a href="#" class="card-footer-item">Delete</a>
        </footer>
    </div>
    <br>
</div>
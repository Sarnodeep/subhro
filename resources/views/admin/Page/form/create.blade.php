<div class="message-body">
    {!! Form::open(['url'=>url('admin/pages'),'files'=> true]) !!}
    @include('errors.list')
    @include('admin.Page.form._form')
    {!! Form::close()!!}
</div>
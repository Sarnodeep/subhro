<div class="field">
    <div class="control">
        {!! Form::text('title', null, ['class'=>'input is-info', 'placeholder' => 'Title']) !!}
    </div>
</div>
<hr class="navbar-divider">

<div class="field">
    <div class="control">
        {!! Form::submit('Submit', ['class'=>'button is-info']) !!}
    </div>
</div>
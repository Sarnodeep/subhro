@extends('layouts.adminLayout')
@section('content')

    <div class="columns">
        <div class="column is-two-thirds">
            {!! Form::open(['method'=> 'patch', 'url' => url('admin/footer')]) !!}

            <div class="field">
                <div class="control has-icons-left">
                    {!! Form::text('facebookUrl', env('facebookUrl'), ['class'=>'input ', 'placeholder' => 'facebook']) !!}
                    <span class="icon is-small is-left has-text-link">
                        <i class="fa fa-facebook-square"></i>
                    </span>
                </div>
            </div>

            <div class="field">
                <div class="control has-icons-left">
                    {!! Form::text('youtubeUrl', env('youtubeUrl'), ['class'=>'input', 'placeholder' => 'youtube']) !!}
                    <span class="icon is-small is-left has-text-danger">
                        <i class="fa fa-youtube-square"></i>
                    </span>
                </div>
            </div>
            <div class="field">
                <div class="control has-icons-left">
                    {!! Form::text('twitterUrl', env('twitterUrl'), ['class'=>'input', 'placeholder' => 'twitter']) !!}
                    <span class="icon is-small is-left has-text-info">
                        <i class="fa fa-twitter-square"></i>
                    </span>
                </div>
            </div>
            <div class="field">
                <div class="control has-icons-left">
                    {!! Form::text('googlePlusUrl', env('googlePlusUrl'), ['class'=>'input', 'placeholder' => 'google plus']) !!}
                    <span class="icon is-small is-left has-text-danger">
                        <i class="fa fa-google-plus-square"></i>
                    </span>
                </div>
            </div>
            <hr class="navbar-divider">

            <div class="field">
                <div class="control">
                    {!! Form::submit('Submit', ['class'=>'button is-info']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@extends('layouts.adminLayout')
@section('content')

    <div class="columns">

        <div class="column is-two-thirds">

            @foreach($pages as $id=>$page)
                @if(!($id%2)) <div class="columns"> @endif
                    <div class="column">
                        <div class="card">
                                <header class="card-header">
                                    <p class="card-header-title">
                                        {!! $page->title !!}
                                    </p>

                                    <a href="#" class="card-header-icon" aria-label="more options">
                            <span class="icon">
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </span>
                                    </a>
                                </header>

                                <div class="card-content">
                                    <figure class="image is-128x128">
                                        <img src="https://bulma.io/images/placeholders/256x256.png"
                                             alt="Placeholder image">
                                    </figure>
                                    <div class="content">
                                        <br>
                                        <time> {!! $page->created_at->diffForHumans() !!}</time>
                                    </div>
                                </div>

                                <footer class="card-footer">
                                    <a href="{!! url($page->slug) !!}" class="card-footer-item">View</a>
                                    <a href="{!! url("admin/pages/$page->id/edit") !!}" class="card-footer-item">Edit</a>
                                    <a href="#" class="card-footer-item">Delete</a>
                                </footer>
                            </div>
                        <br>
                    </div>
                @if( ($id%2) || $id==(count($pages)-1)) </div> @endif
            @endforeach

        </div>

        <div class="column">
            <div class="message">
                <div class="message-header">
                    <p>Add Page</p>
                </div>
                @include('admin.Page.form.create')
            </div>
        </div>

    </div>
@endsection


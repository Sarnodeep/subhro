@extends('layouts.adminLayout')
@section('content')
    <div class="columns">
        <div class="column is-half">

            <article class="message">

                <div class="message-header">
                    <p>Slider</p>
                </div>

                <div class="message-body">

                    {!! Form::open(['url'=>action("Admin\\PageSectionController@update",[$page->id, $section_id]),'method' => 'PATCH','files'=>true]) !!}

                    @include('errors.list')

                    @include("admin.forms.partials.section_$section_id")

                    <hr class="navbar-divider">

                    <div class="field">
                        <div class="control">
                            {!! Form::submit('Submit', ['class'=>'button is-info']) !!}
                        </div>
                    </div>

                    {!! Form::close()!!}

                </div>

            </article>

        </div>

        <div class="column is-half">
            @include("admin.forms.partials.preview.section_$section_id")
        </div>

    </div>
@endsection
<div class="columns">
    <div class="column is-two-thirds">

        <article class="message">
            <div class="message-header">
                <p>Slider</p>
            </div>

            <div class="message-body">
                {!! Form::open(['method'=> 'patch', 'url' => url("admin/pages/$page->id/section/$section_id"),'files'=>true]) !!}

                @include('errors.list')

                <div class="field">
                    <div class="control">
                        {!! Form::textarea('description', getSectionData($page, $section_id), ['class'=>'textarea', 'rows' => '20']) !!}
                    </div>
                </div>

                <hr class="navbar-divider">

                <div class="field">
                    <div class="control">
                        {!! Form::submit('Submit', ['class'=>'button is-info']) !!}
                    </div>
                </div>

                {!! Form::close()!!}
            </div>

        </article>

    </div>
    <div class="column">

    </div>
</div>


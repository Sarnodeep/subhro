@foreach(getSectionData($page, $section_id) as $key => $data)

    <div class="box">
        <article class="media">
            <div class="media-left">

                <figure class="image is-64x64">
                    <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                </figure>

            </div>

            <div class="media-content">

                <div class="content">
                    <p>
                        <strong>Heading 1</strong> <small>{!! getPageSectionProperty($data, 'heading_1') !!}</small>
                        <br>
                        <strong>Heading 2</strong> <small>{!! getPageSectionProperty($data, 'heading_2') !!}</small>
                        <br>
                        <strong>Button</strong> <small>{!! getPageSectionProperty($data, 'heading_1') !!}</small>
                    </p>
                </div>

                <nav class="level is-mobile">

                    <div class="level-left">

                        <a class="button is-primary" href="{!! action("Admin\\PageSubSectionController@edit",[$page->id, $section_id, $key]) !!}">
                            <span class="icon is-small"><i class="fa fa-pencil"></i></span> &nbsp;Edit
                        </a>
                        &nbsp;
                        <a class="button is-danger" href="#">
                            <span class="icon is-small"><i class="fa fa-trash"></i></span> &nbsp; Delete
                        </a>

                    </div>

                </nav>
            </div>
        </article>

    </div>
@endforeach
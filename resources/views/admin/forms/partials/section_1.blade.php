<div class="field">
    <div class="control">
        {!! Form::text('heading_1',  getSubSectionPropertyByIndex($page, $section_id, isset($subSection_id)? $subSection_id : false, 'heading_1'), ['class'=>'input is-info', 'placeholder' => 'Heading 1']) !!}
    </div>
</div>

<div class="field">
    <div class="control">
        {!! Form::text('heading_2', getSubSectionPropertyByIndex($page, $section_id, isset($subSection_id)? $subSection_id : false, 'heading_2'), ['class'=>'input is-info' ,'placeholder' => 'Heading 2']) !!}
    </div>
</div>

<div class="field">
    <div class="control">
        {!! Form::text('button', null, ['class'=>'input is-info' ,'placeholder' => 'Button']) !!}
    </div>
</div>

<div class="field">
    <div class="control">
        {!! Form::file('image', null, ['class'=>'file-input']) !!}
    </div>
</div>






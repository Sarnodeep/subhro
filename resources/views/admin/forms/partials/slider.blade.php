<div class="columns">
    <div class="column is-two-thirds">

        <article class="message">
            <div class="message-header">
                <p>Slider</p>
            </div>

            <div class="message-body">
                {!! Form::open(['url'=>url('admin/slider-section'),'files'=>true]) !!}

                @include('errors.list')

                {!! Form::hidden('page_id', 1) !!}
                {!! Form::hidden('section_type_id', 1) !!}

                <div class="field">
                    <div class="control">
                        {!! Form::text('heading_1', null, ['class'=>'input is-info', 'placeholder' => 'Heading 1']) !!}
                    </div>
                </div>

                <div class="field">
                    <div class="control">
                        {!! Form::text('heading_2', null, ['class'=>'input is-info' ,'placeholder' => 'Heading 2']) !!}
                    </div>
                </div>

                <div class="field">
                    <div class="control">
                        {!! Form::file('image', null, ['class'=>'file-input']) !!}
                    </div>
                </div>

                <hr class="navbar-divider">

                <div class="field">
                    <div class="control">
                        {!! Form::submit('Submit', ['class'=>'button is-info']) !!}
                    </div>
                </div>
                {!! Form::close()!!}
            </div>

        </article>

    </div>
    <div class="column">

    </div>
</div>


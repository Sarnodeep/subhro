<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

</head>
<body>
    <span id="app" v-cloak>
        <div class="columns is-gapless is-multiline">

            <div class="column is-2 is-hidden-mobile">
                <section class="hero is-light is-fullheight">
                    <div class="hero-body">
                        <div class="container">
                            @include('layouts.partials.sidebar')
                        </div>
                    </div>
                </section>
            </div>

            <div class="column">
                @include('layouts.partials.adminHeader')
                <br>
                <div class="container is-fluid">
                    @yield('content')
                </div>
            </div>

        </div>
    </span>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

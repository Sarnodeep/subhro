@if($subCategory->hasSubcategory())



    <li class="dropdown-submenu">
        <a class="test" tabindex="-1" href="#">{!! $subCategory->title !!} <span class="caret"></span></a>

        <ul class="dropdown-menu">
            @foreach($subCategory->subCategories as $item)
                <li><a tabindex="-1" href="{!! url($item->slug) !!}">{!! $item->title !!}</a></li>
            @endforeach
        </ul>
    </li>

@else
    <li><a href="{!! $subCategory->slug !!}">{!! $subCategory->title !!}</a></li>
@endif
<aside class="menu">
    <p class="menu-label">
        General
    </p>
    <hr>
    <ul class="menu-list">
        <li><a href="{!! url('admin/') !!}">Dashboard</a></li>
    </ul>
    <br>

    <p class="menu-label">
        Administration
    </p>
    <hr>
    <ul class="menu-list">
        <li><a href="{!! url('admin/pages') !!}">Pages</a></li>
        <li><a href="{!! url('admin/messages') !!}">Messages</a></li>
        <li><a href="{!! url('admin/footer') !!}">Footer</a></li>
    </ul>
    <br>
</aside>
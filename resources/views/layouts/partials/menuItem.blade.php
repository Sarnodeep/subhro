@if($category->hasSubcategory())


    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
           aria-expanded="false">{!! $category->title !!} <span class="caret"></span></a>

        <ul class="dropdown-menu">
            @foreach($category->subCategories as $subCategory)
                @include('layouts.partials.subMenuItem')
            @endforeach
        </ul>

    </li>

@else

    <li><a href="{!! url($category->slug) !!}">{!! $category->title !!}</a></li>

@endif
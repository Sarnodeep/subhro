<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{!! env('APP_NAME') !!}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Custom fonts for this template -->
    {{--<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">--}}
            <!-- Custom styles for this template -->
    <link href="{!! asset('css/app.css') !!}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css">
    <link rel="stylesheet" href="http://startbootstrap-agency.test/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://startbootstrap-agency.test/css/agency.min.css">

</head>

<body id="page-top">
<span id="app">
    <!-- Navigation -->
    @include('layouts.partials.navbar')
    <!-- Header -->
    @include('layouts.partials.header')


    @include('sections.section_2')

{{--    @include("sections.section_$section")--}}

    @include('sections.footer')
</span>


<script src="{!! asset('js/app.js') !!}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js"></script>

</body>
</html>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{!! env('APP_NAME') !!}</title>
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<span id="app">
</span>
@include('layouts.partials.header')
@include('layouts.partials.navBar')
<script src="{!! asset('js/app.js') !!}"></script>
</body>
</html>


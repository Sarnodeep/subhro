@extends('layouts.app2Layout')

@section('adminContent')


    <div class="container is-fluid">
        <div class="columns">
            <div class="column is-6 is-offset-3">
                <div class="box">
                    <h1 class="title is-3">Login</h1>
                    <hr>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="field">
                            <label class="label ">Email</label>

                            <input type="email" class="input {{ $errors->has('email') ? ' is-danger' : '' }}"
                                   name="email" value="{{ old('email') }}" placeholder="yourname@example.com" required
                                   autofocus>

                            @if ($errors->has('email'))
                                <p class="help is-danger">{{ $errors->first('email') }}</p>
                            @endif
                        </div>

                        <div class="field">
                            <label class="label">Password</label>
                            <input type="password" class="input {{ $errors->has('password') ? ' is-error' : '' }}"
                                   name="password">
                            @if ($errors->has('password'))
                                <p class="help is-danger">{{ $errors->first('password') }}</p>
                            @endif
                        </div>


                        <div class="field">
                            <button class="button is-link is-outlined" type="submit">Login</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

<?php

/**
 * set environment variable
 *
 * @param $envKey
 * @param $envValue
 */
function setEnvironmentValue($envKey, $envValue)
{
    abort_if(!in_array($envKey, ['facebookUrl', 'twitterUrl', 'youtubeUrl', 'youtubeUrl', 'googlePlusUrl']), 404);

    $envFile = app()->environmentFilePath();
    $str = file_get_contents($envFile);

    $oldValue = $oldValue = env($envKey);

    $str = str_replace("{$envKey}={$oldValue}", "{$envKey}={$envValue}", $str);

    $fp = fopen($envFile, 'w', 777);
    fwrite($fp, $str);
    fclose($fp);
}

/**
 * return pages list
 *
 * @return mixed
 */
function getPagesList()
{
    return \App\Models\Page::get()->pluck('title', 'id');
}

/**
 * create new order for the sections
 *
 * @param $sections
 * @param bool $order
 * @return bool
 */
function getOrder($sections, $order = false)
{
    if ($order) {
        return $order;
    }
    return $sections->count() + 1;
}

/**
 * @param \App\Models\Page $page
 * @param $section_id
 * @return array|mixed
 */
function getSectionData(\App\Models\Page $page, $section_id)
{
    $section = $page->sections()->find($section_id);
    if (is_null($section)) {
        return [];
    }
    return (new \App\Helpers\PageSection($section))->getData();
}

/**
 * returns subSection
 *
 * @param \App\Models\Page $page
 * @param \App\Models\SectionType $section
 * @param $index
 * @return array
 */
function getSubSectionByIndex(\App\Models\Page $page, $section_id, $index = null)
{
    if (!is_null($index)) {
        $section = $page->sections()->find($section_id);
        if (is_null($section)) {
            return [];
        }
        return (new \App\Helpers\PageSection($section))->getSubSection($index);
    }
    return '';
}

function getSubSectionPropertyByIndex(\App\Models\Page $page, $section_id, $index = null, $property = null)
{
    if (is_null($property)) {
        return '';
    }
    return getSubSectionByIndex($page, $section_id, $index)[$property];
}



//
///**
// * add a new slider
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSlider($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
//
///**
// * add a new section 1
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSection1($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
//
///**
// * add a new section 1
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSection2($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
//
///**
// * add a new section 3
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSection3($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
//
///**
// * add a new section 4
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSection4($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
//
///**
// * add a new section 5
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSection5($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
//
///**
// * add a new section 6
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSection6($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
//
///**
// * add a new section 7
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSection7($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
//
///**
// * add a new section 8
// *
// * @param $page_id
// * @param $section_type_id
// * @param $heading_1
// * @param $heading_2
// * @param $image
// * @param int $order
// * @return mixed
// */
//function addSection8($page_id, $section_type_id, $heading_1, $heading_2, $image, $order = 0)
//{
//    return self::addSection($page_id, $section_type_id, $order, [
//        'heading_1' => $heading_1,
//        'heading_2' => $heading_2
//    ]);
//}
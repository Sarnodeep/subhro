<?php

namespace App\Helpers;

use Illuminate\Http\Request;

class PageSection
{
    protected $PageSection;

    /**
     * PageSection constructor.
     *
     * @param null $section
     */
    public function __construct($section = null)
    {
        $this->PageSection = $section;
    }

    /**
     * returns the pivot data
     *
     * @return mixed
     */
    public function pivot()
    {
        return $this->PageSection->pivot;
    }

    /**
     * returns the data part of the pivot table
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->pivot()->data;
    }

    /**
     *
     * @param $name
     * @return string
     */
    public static function getProperty($data, $param)
    {
        return $data[$param];
    }


    /**
     * returns particular sub Section
     *
     * @param $id
     * @return array
     */
    public function getSubSection($id)
    {
        return isset($this->getData()[$id]) ? $this->getData()[$id] : [];
    }


    /**
     * returns string if class called directly
     * @return string
     */
    public function __toString()
    {
        return '';
    }

    /**
     * prepare data to be saved
     *
     * @param Request $request
     * @return mixed
     */
    public static function prepareData(Request $request)
    {
        return $request->except(['_method', '_token']);
    }
}
<?php

namespace App\Models;

use App\Helpers\PageSection;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Page extends Model
{
    use Sluggable, ModelTrait;

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $casts = [
        'sections.data' => 'array'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * a page can have many sections
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sections()
    {
        return $this->belongsToMany(SectionType::class)
            ->using(PageSectionType::class)
            ->withPivot(['order', 'data'])
            ->withTimestamps();
    }

    /**
     * selects all the sections
     *
     * @param $page_id
     * @return mixed
     */
    private static function selectSection($page_id)
    {
        return self::findOrFail($page_id)
            ->sections();
    }

    /**
     * update sections
     *
     * @param $page_id
     * @param $section_type_id
     * @param array $data
     * @param bool $order
     * @return mixed
     */
    public static function updateSection($page_id, $section_type_id, Request $request, $order = false)
    {
        $sections = self::selectSection($page_id, $section_type_id);
        return is_null($sections->find($section_type_id)) ? self::attachSection($section_type_id, $sections, $request, getOrder($sections, $order))
            : self::syncSection($section_type_id, $sections, $request, getOrder($sections));
    }

    /**
     * attach new section with a page
     *
     * @param $section_type_id
     * @param $section
     * @param $data
     * @param $order
     * @return mixed
     */
    private static function attachSection($section_type_id, $section, Request $request, $order)
    {
        return $section->attach($section_type_id, [
                'order' => $order,
                'data' => [PageSection::prepareData($request)]
            ]
        );
    }

    /**
     * sync sections
     *
     * @param $section_type_id
     * @param $section
     * @param $data
     * @param $order
     * @return mixed
     */
    private static function syncSection($section_type_id, $section, Request $request, $order)
    {
//        return dd($section->get()->find($section_type_id)->pivot->data);
        return $section->sync($section_type_id, [
                'order' => $order,
                'data' => PageSection::prepareData($request)
            ]
        );
    }

    /**
     * delete particular section from page
     *
     * @param $page_id
     * @param $section_type_id
     * @return mixed
     */
    public static function removeSection($page_id, $section_type_id)
    {
        return self::selectSection($page_id)->detach($section_type_id);
    }
}

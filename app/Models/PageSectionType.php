<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PageSectionType extends Pivot
{
    protected $casts = [
        'data' => 'collection'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

}

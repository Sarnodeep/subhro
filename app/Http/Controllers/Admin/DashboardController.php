<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Footer page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function footer()
    {
        return view('admin.Page.footer');
    }

    /**
     * @param Request $request
     * @return array|string
     */
    public function setFooter(Request $request)
    {
        collect($request->except(['_method', '_token']))->each(function ($item, $key) {
            setEnvironmentValue($key, $item);
        });
        return redirect()->back();
    }
}

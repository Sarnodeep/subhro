<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageSectionController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param $page_id
     * @param $section_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($page_id, $section_id)
    {
        $page = Page::findOrFail($page_id);
        return view('admin.forms.index', compact('page', 'section_id'));
    }

    public function store(Request $request)
    {
        return $request->input();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $page_id, $section_id)
    {
        Page::updateSection($page_id, $section_id, $request);
        return redirect("admin/pages/$page_id/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

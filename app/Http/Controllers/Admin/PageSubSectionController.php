<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class PageSubSectionController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param $page_id
     * @param $section_id
     * @param $subSection_id
     * @return \Illuminate\Http\Response
     */
    public function edit($page_id, $section_id, $subSection_id)
    {
        $page = Page::findOrFail($page_id);
        $page->sections()->findOrFail($section_id);
        return view('admin.forms.edit', compact('page', 'section_id', 'subSection_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $page_id
     * @param $section_id
     * @param $subSection_id
     * @return array|string
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $page_id, $section_id, $subSection_id)
    {
        return $request->input();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

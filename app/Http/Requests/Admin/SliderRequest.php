<?php

namespace App\Http\Requests\Admin;

use App\Models\SectionType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page_id' => 'required|int',
            'section_type_id' => ['required',
                Rule::in(SectionType::get()->pluck('id')->toArray())
            ],
            'heading_1' => 'required|string|max:100',
            'heading_2' => 'required|string|max:255',
            'image' => 'required|image'
        ];
    }
}

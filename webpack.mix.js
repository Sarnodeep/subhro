let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js(['resources/assets/js/app.js',
//        'node_modules/jquery-easing/jquery.easing.1.3.js',
//        'resources/assets/theme/js/jqBootstrapValidation.js',
//        'resources/assets/theme/js/contact_me.js',
//        'resources/assets/theme/js/agency.min.js'
//    ], 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css')

mix.sass('resources/assets/sass/admin.scss', 'public/css/admin.css');
    //.styles([
    //    'public/css/app.css',
        //'resources/assets/theme/css/agency.min.css'
    //], 'public/css/app.css')
<?php
Route::get('/', 'DashboardController@index');
Route::resource('pages', 'PageController');
Route::resource('pages/{page_id}/section', 'PageSectionController');

Route::get('footer', 'DashboardController@footer');
Route::patch('footer', 'DashboardController@setFooter');

Route::get('pages/{page_id}/section/{section_id}/sub-section/{subSection_id}/edit', 'PageSubSectionController@edit');
Route::patch('pages/{page_id}/section/{section_id}/sub-section/{subSection_id}/edit', 'PageSubSectionController@update');
Route::delete('pages/{page_id}/section/{section_id}/sub-section/{subSection_id}', 'PageSubSectionController@delete');
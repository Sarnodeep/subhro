<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();
Route::any('register', function () {
   abort(404);
});


//Route::get('foo', function () {
//
//    return [
//        [
//            'heading_2' => 'Welcome',
//            'heading_1' => 'It\'s Nice tp meet you ',
//            'link' => [
//                'value' => 'Tell me more',
//                'href' => '#services'
//            ],
//            'image' => 'someting.jpg'
//        ],
//        [
//            'heading_2' => 'Welcome',
//            'heading_1' => 'It\'s Nice tp meet you ',
//            'link' => [
//                'value' => 'Tell me more',
//                'href' => '#services'
//            ],
//            'image' => 'someting.jpg'
//        ],
//        [
//            'heading_2' => 'Welcome',
//            'heading_1' => 'It\'s Nice tp meet you ',
//            'link' => [
//                'value' => 'Tell me more',
//                'href' => '#services'
//            ],
//            'image' => 'someting.jpg'
//        ]];
//});
